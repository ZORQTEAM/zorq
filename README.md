<!--
*** Thanks for checking out this README Template. If you have a suggestion that would
*** make this better, please fork the repo and create a pull request or simply open
*** an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
***
***
***
*** To avoid retyping too much info. Do a search and replace for the following:
*** github_username, repo_name, twitter_handle, email
-->

<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]



<!-- PROJECT LOGO -->
<!-- 
<br />
<p align="center">
  <a href="https://github.com/ZORQTEAM/ZORQ">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>
  
 -->

  <h3 align="center">ZORQ</h3>
<!-- 
  <p align="center">
    ZORQ DESCRIPTION
    <br />
    <a href="https://github.com/ZORQTEAM/ZORQ"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://github.com/ZORQTEAM/ZORQ">View Demo</a>
    ·
    <a href="https://github.com/ZORQTEAM/ZORQ/issues">Report Bug</a>
    ·
    <a href="https://github.com/ZORQTEAM/ZORQ/issues">Request Feature</a>
  </p>
</p>

 -->
<!--  --> 

<!-- TABLE OF CONTENTS -->
## Table of Contents
* [About the Project](#about-the-project)
<!--  
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
  --> 
* [Usage](#usage)
* [Roadmap](#roadmap)

<!--  
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)
--> 



<!-- ABOUT THE PROJECT -->
## About The Project
ZORQ (Zero Operator ReQuired) is a Game Development Based Learning and Gamification Framework (GDGF) in which space ships navigate a 2D game interstellar universe filled with objects which affect a ship either positively or negatively. Examples of game objects that have been used include fuel, shields, mines, black holes, ship-jump portals, electromagnetic pulse, bullets and lasers. Students adjust the GDGF architecture as needed each semester, and then implement code to automatically control their own ship in a gamified process. 

In the current version of ZORQ, ships earn points for each frame in which they remain active/alive, and also by gathering resources which award bonus points. Remaining active is the primary means by which ships maximize points. Negative encounters cause a ship to be deactivated for five seconds, after which they respawn in a different location. Examples of negative encounters include being successfully targeted by another ship, running into an obstacle, or getting sucked into a black hole. Ships can target other ships, and if successful, steal a percentage of the score from the attacked ship. Engagements between ships generally favor the ship with more resources (e.g., fuel, bullets, shield energy, etc.) or a superior strategy. The particulars of these engagements can vary greatly by semester, along with the resources/objects present in the game, depending on how students want the game to behave. 

[![ZORQ Screen Shot](https://gitlab.com/ZORQTEAM/zorq/-/blob/main/ZORQ.png)]

<!--  
### Built With

* []()
* []()
* []()
--> 
 

<!-- GETTING STARTED -->
<!-- ## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* npm
```sh
npm install npm@latest -g
```

### Installation

1. Clone the repo
```sh
git clone https://github.com/ZORQTEAM/ZORQ.git
```
2. Install NPM packages
```sh
npm install
```

--> 

<!-- USAGE EXAMPLES -->
## Usage
At initial startup, the user is met with a settings screen that provides various game simulation options such as multi-threading, item spawn rate, universe size, graphical scrolling background toggle, and headless mode. Next, the user selects the ship controllers to be used in the simulation. 
Afterward, a selection screen appears that dynamically displays the names of available ship controllers through the use of reflection.  To start game play, the user selects the ship controllers to use during that iteration from that list of controllers. The user may select as many controllers as desired. Once selected, the controllers are internally connected to ships, one controller per ship, the game engine, 
and the game begins. If headless mode was not selected, then graphics are set up before the game begins. During game time, the game space and elements are depicted with moving images.

In the upper left of the screen, stats are displayed including controls, frame rate, and a listing of the active ship controllers and their scores. The frame rate can be adjusted via the keyboard, allowing for rapid simulation, with one engine cycle (i.e., game state) per displayed frame. The game runs indefinitely until the program is closed.

If headless mode is selected, the graphical display is replaced by console output which periodically shows stats including the game frame/cycle rate. In headless mode, the frame rate is uncapped and the simulation runs as fast as the hardware permits. Speeds in excess of 100k cycles per second are possible. This holds the capability to generate massive amounts of data quickly as the frame rate is much higher than in graphical mode. Options for logging relevant data for deep learning are being considered. In headless mode, the only displays are an empty window that you can close the application with and a console output for testing.

An overview of the ZORQ classes most relevant to this discussion appears in the UML class diagram shown at
(https://gitlab.com/ZORQTEAM/zorq/-/blob/main/StructureDiagram.png). The classes are separated and color coded based on their level of abstraction, where level 0 is the most abstract and level 3 is the least abstract. Objects at higher levels know about objects at lower levels. The Engine manages the game, and contains the main game loop which advances the game state while maintaining a steady frame rate. The Universe holds the less abstract game elements, and steps forward to the next game state when triggered by the Engine. Elements in the Universe are special types of FlyingObject.

Each ship is connected to a custom ShipController, developed by the students, which dictate how the ship behaves. Each controller has access to the Universe and thus all the elements in the game space. Students decide and implement what they want their ship to do (via the controller) based on the state of the game elements in the Universe.

There are a few main components to the scoring system. As ships move around the game space, their scores will gradually increase. If a ship is destroyed, it then loses some of its score. If it was destroyed by an opposing ship, then the opposing ship gains a portion of the destroyed ship's score. Because a ship's resources are limited, students often develop very complex strategies.

There are several game elements described as bonuses, as shown graphically in the screenshopt and listed as classes in the UML class diagram. Fuel bonus objects resupply ships with a limited amount of fuel which is otherwise consumed as a ship accelerates. Bullet bonuses refill and upgrade a ship’s bullet supply; as ships have the ability to shoot bullets. Each ship has a metered shield that is decremented when powered on to deflect dangerous objects. Shield bonuses refill this meter. Ships are allowed to teleport to another location on demand using ship jump bonus objects (shown as blue circles with white centers). Ships can also shoot lasers and laser bonus objects (shown as flaming wands) refill a ship’s laser supply. Electromagnetic pulse (EMP) bonus objects (shown as three green circles) give a ship EMP blast capabilities.

Points bonus objects (shown as dollar signs) give ships that pick them up points to improve their score. Magnet bonus objects cause a ship to pull in nearby bonus objects for a short amount of time. 

Several objects have negative outcomes for ships. Bombs and bullets are negative bonus objects that
either damage a ship's shield, or destroy a ship if touched. Much like bombs, bullets are objects that damage an opposing ship's shield or destroy the ship if hit. The distinguishing factor between bombs and bullets is that bullets originate from opposing ships. Lasers are a special type of bullet that move very fast, making it harder for opposing ships to react. A ship hit by an EMP loses its ability to shield and shoot objects for a short amount of time during which it becomes vulnerable to attack. Black holes are hazards that pull in ships and destroy them. Black holes also attract bullets and other objects that get close to them, as shown in the screenshot.

<!-- ROADMAP -->
## Roadmap

ZORQ was refactored to better separate the model and view. In addition to showing best practices, another benefit of improving the design was the opportunity to speed up the frame rate. Rather than simply displaying the game at a steady frame rate, ZORQ was modified so that it can be adjusted to run as fast as the computer can support. Over 1000 simulated frames per second (fps) in graphical mode are possible depending on the hardware, the number of controllers, and the CPU demands of the controller activities/strategies. This allows students to quickly see how well their controllers will behave over time, and supports stress testing.

The architecture was stress tested at high frame rates using a profiler to identify and resolve issues such as logical errors and memory leaks caused by failing to remove items from collections. Profilers can conduct a variety of dynamic software analyses and are available for most popular environments, dating back to 1982 (Graham,1982).

The headless mode was also a recent addition. Speeds in excess of 100k cycles per second are possible, allowing for further stress testing and for the fast generation of training data needed to develop future controllers using machine-learning/deep-learning approaches. 

A multi-threaded mode was added, in which controllers running in separate threads. Typically, simulations are run with around 15-20 controllers. The possible benefits of the multi-threaded mode need to be investigated further.

The architecture was reviewed from a coupling and cohesion perspective using CodeMR. (Coupling is a measure of the interconnectedness of modules within a system, while cohesion is a measure of the degree with which components within a module are related to each other. High coupling complicates system maintenance as updates to one class are more likely to impact other classes with a cascading effect possible. In object-oriented software, for the sake of maintainability and scalability, it is generally prefered to have low coupling and high cohesion.) 

Because of the quick inaugural development and iterative updates, ZORQ's architecture was found to be somewhat weaker in these areas. Several instances of low cohesion and high coupling in ZORQ were subsequently discussed and repaired in the SE course. For example, the graphics were separated from game objects allowing game objects to maintain only the logic of how they should function rather than how they should look. These types of improvements make way for new features to be more easily added to the architecture. The current cohesion and coupling results can be seen at (https://gitlab.com/ZORQTEAM/zorq/-/blob/main/Charts.png), and according to the results, further improvements are possible. 
 
Additional perfective changes were made such as profiling ZORQ to identify code that could be sped up, e.g., replacing the precise distance formula which uses the slow "sqrt" math function with distance comparisons using the distance squared calculation.  According to Swanson, perfective maintenance is maintenance performed to eliminate processing inefficiencies, enhance performance, or improve maintainability.

<!-- 
See the [open issues](https://github.com/ZORQTEAM/ZORQ/issues) for a list of proposed features (and known issues).
-->

<!-- CONTRIBUTING -->
<!-- 
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

-->

<!-- LICENSE -->
<!-- 
## License

Distributed under the XXX License. See `LICENSE` for more information.

-->

<!-- CONTACT -->
<!-- 
## Contact

Your Name - email
-->

<!-- 
Project Link: [https://github.com/ZORQTEAM/ZORQ](https://github.com/ZORQTEAM/ZORQ)
-->

<!-- ACKNOWLEDGEMENTS -->
<!-- 
## Acknowledgements

* []()
* []()
* []()

--> 



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/ZORQTEAM/repo.svg?style=flat-square
[contributors-url]: https://github.com/ZORQTEAM/repo/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/ZORQTEAM/repo.svg?style=flat-square
[forks-url]: https://github.com/ZORQTEAM/repo/network/members
[stars-shield]: https://img.shields.io/github/stars/ZORQTEAM/repo.svg?style=flat-square
[stars-url]: https://github.com/ZORQTEAM/repo/stargazers
[issues-shield]: https://img.shields.io/github/issues/ZORQTEAM/repo.svg?style=flat-square
[issues-url]: https://github.com/ZORQTEAM/repo/issues
[license-shield]: https://img.shields.io/github/license/ZORQTEAM/repo.svg?style=flat-square
[license-url]: https://github.com/ZORQTEAM/repo/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/ZORQTEAM
[product-screenshot]: images/screenshot.png
